<html lang="en">
	<head>
		<title> </title>
	</head>

	<body>


  <div class="well">

    <div class="text-center">
      <h2><img src="static/img/logo.jpg" style="position: absolute; left:10px;top:10px;"> {{ $input['title'] }}  <span style="position: absolute; left:550px;font-size:.6em;font-weight:default"> {{ date('F jS, Y') }}</span> </h2>
      <h3> Project Planning Form </h3>

      <p> {{ $input['notes'] }} </p>
      <p> Teams affected: {{ $input['teams'] }} </p>

    </div>

    <p class="text-center">
      Start date: {{ $input['startdate'] }}

      <span style="margin-left:50px;text-align:right;">
        Target end date: {{ $input['enddate'] }}
      </span>
    </p>

  </div>
  @foreach($input['phases'] as $phase)

		<p class="text-center" style="position: relative; top:35px;"><strong>{{ $phase->title }}</strong></p>


			<span style="font-size:1.1em;wdith=30%"> Phase {{ $phase->id }} </span>
			<br>
      <p style="position: relative; left: 35px;"> {{ $phase->desc }} </p>

      <br>

			<span style="">
				<strong>Target completion:</strong> {{ $phase->date }}
			</span>

			<span style="position: relative; margin-left: 35px;">
				<strong>Lead:</strong> {{ $phase->lead }}
			</span>

      <span style="position: relative; margin-left: 35px;">
				<span style="text-align:right"><strong>Support:</strong> {{ $phase->support }}</span>
      </span>

      <hr>
  @endforeach




<style>

@import url(http://fonts.googleapis.com/css?family=Oswald:400,300,700);

body {
  font-family: 'Oswald', sans-serif;
}

.row {
  margin-right: -15px;
  margin-left: -15px;
}

.row:before,
.row:after,{
  display: table;
  content: " ";
}

.row:after, {
  clear: both;
}

.text-center{
  text-align: center;
}

.pull-right {
  text-align: right;
}

.well {
  min-height: 20px;
  padding: 5px;
  margin-bottom: 10px;
  background-color: #C2E1F0;
  border: 1px solid #C2E1F0;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
}

.col-xs-6{
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
  float: left;
  width: 50%;
}

hr{
  height: 0;
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
          margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #000;
}

</style>


	</body>


</html>
