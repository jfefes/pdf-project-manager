@extends('layouts.master')

@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">


  <div class="container" style="margin-top:20px">

    <form method="POST" action="/equipment/pdf/create">
      <div class="row form-group">
        <div class="col-xs-6">
          Title: <input type="text" class="form-control" name="title" placeholder="Proposal title"/>
        </div>
      </div>

      <div class="well">
        <h5 class="text-center">Equipment Proposal</h5>

        <div class="row">
          <div class="col-xs-6">
            Description, goals, etc:<br>
            {{ Form::textarea('notes') }}
          </div>


          <div class="col-xs-6">
            Teams affected:<br>
            {{ Form::textarea('teams') }}
          </div>

        </div>

        <br>
      </div>


      <div class="well">
        <h4>Item 1</h4>

        <br>

        <div class="row">
          <div class="col-xs-6 col-sm-4">
            Cost: <input type="text" class="form-control" name="phase-cost[]" placeholder="Cost"/>
          </div>

          <div class="col-xs-6">
            Name: <input type="text" class="form-control" name="phase-title[]" placeholder="Item Name"/>
          </div>
      </div>

      <br>

      Phase description: <br>
      {{ Form::textarea('phase-desc[]') }}

      <div class="row">
        <div class="col-xs-6 col-sm-4">
          Image (url): <input type="text" class="form-control" name="phase-img[]" placeholder="img"/>
        </div>

        <div class="col-xs-6 col-sm-4">
          Source: <input type="text" class="form-control" name="phase-source[]" placeholder="Source"/>
        </div>

      </div>
    </div>


      <div id="phases">

      </div>

      <input type="button" class="btn btn-info"value="Add another Item" onClick="addInput('phases');">


      <br>
      <input type="hidden" id="count" name="count" value="1">
      <br>
      <input type="submit" class="btn btn-success" value="Create"/>
    </form>

<script>
  var counter = 1;
  var limit = 10;
  function addInput(divName){
       if (counter == limit)  {
            alert("You have reached the limit of adding " + counter + " inputs");
       }
       else {
 var content =  "<div class='row'>";

     content+=    "<div class='col-xs-6 col-sm-4'>";
     content+=      "Cost: <input type='text' class='form-control' name='phase-cost[]' placeholder='Cost'/>";
     content+=    "</div>";

     content+=  "<div class='col-xs-6'>";
     content+=    "Name: <input type='text' class='form-control' name='phase-title[]' placeholder='Item Name'/>";
     content+=  "</div>";

     content+= "</div>";

     content+= "<br>";

     content+= "Item description: <br>";
     content+= "<textarea name='phase-desc[]' cols='50' rows='10'></textarea>";

     content+= "<div class='row'>";

     content+= "        <div class='col-xs-6 col-sm-4'>";
     content+= "          Image (url): <input type='text' class='form-control' name='phase-img[]' placeholder='img'/>";
     content+= "        </div>";
     content+= "   <div class='col-xs-6 col-sm-4'>";
     content+= "     Source: <input type='text' class='form-control' name='phase-source[]' placeholder='Source'/>";
     content+= "   </div>";

            var newdiv = document.createElement('div');
            newdiv.className = "well";
            newdiv.innerHTML = "<h4>Item " + (counter+1) + "</h4>";
            newdiv.innerHTML += content;
            document.getElementById(divName).appendChild(newdiv);

            document.getElementById('count').value= (counter+1);
            counter++;
       }

       $(function() {
         $('.datepick').each(function(){
           $(this).datepicker();
         });
     });
  }
</script>

<script>
  $(function() {
    $('.datepick').each(function(){
      $(this).datepicker();
    });
});
</script>


@stop
