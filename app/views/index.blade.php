@extends('layouts.master')

@section('content')

  <div class="container text-center">
    <h3>Do the thing...</h3>
    <div class="well row">
      <a class="btn btn-default" href="/project">Project proposal</a>
      &nbsp; &nbsp;
      <a class="btn btn-default" href="/equipment">Equipment Purchasing</a>
    </div>
  </div>

@stop
