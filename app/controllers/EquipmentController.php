<?php

class Equipment{
	public $id;
	public $cost;
	public $title;
	public $desc;
	public $source;
}

class EquipmentController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('pages.createEquipment');
	}

	public function createPDF(){
		$input = Input::all();

		$input['equipment'] = array();


		for($i=0;$i<$input['count'];$i++){
			$equipment = new Equipment();

			$equipment->id = $i+1;
			$equipment->cost = $input['phase-cost'][$i];
			$equipment->title = $input['phase-title'][$i];
			$equipment->desc = $input['phase-desc'][$i];
			$equipment->img = $input['phase-img'][$i];
			$equipment->source = $input['phase-source'][$i];

			array_push($input['equipment'], $equipment);
		}


		$view = View::make('pdf.equipment', array('input' => $input))->render();

		File::delete(public_path() . "/equipment.html");
    File::put(public_path() . "/equipment.html", $view);

		$pdf = App::make('dompdf');
		$pdf->loadFile(public_path() . '/equipment.html');
		return $pdf->stream();
	}

}
