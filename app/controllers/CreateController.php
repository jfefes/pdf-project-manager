<?php

class Phase{
	public $id;
	public $date;
	public $title;
	public $desc;
	public $lead;
	public $support;
}

class CreateController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('pages.create');
	}

	public function createPDF(){
		$input = Input::all();

		$input['phases'] = array();

		for($i=0;$i<$input['count'];$i++){
			$phase = new Phase();

			$phase->id = $i+1;
			$phase->date = $input['phase-date'][$i];
			$phase->title = $input['phase-title'][$i];
			$phase->desc = $input['phase-desc'][$i];
			$phase->lead = $input['phase-lead'][$i];
			$phase->support = $input['phase-support'][$i];

			array_push($input['phases'], $phase);
		}


		$view = View::make('pdf.template', array('input' => $input))->render();

		File::delete(public_path() . "/project.html");
    File::put(public_path() . "/project.html", $view);

		$pdf = App::make('dompdf');
		$pdf->loadFile(public_path() . '/project.html');
		return $pdf->stream();
	}

}
